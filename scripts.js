const loadBtn = document.querySelector(".js-load");
const btnPlaceHolder = document.querySelector('.load-button-placeholder')
const resultsContainer = document.querySelector(".js-results");
const searchInput = document.querySelector(".js-input");


loadBtn.addEventListener("click", function (e) {
  e.preventDefault();
  const searchValue = searchInput.value.trim().toLowerCase();
  axios.get(`https://api.github.com/users/${searchValue}`)
    .then((data) => resultsContainer.innerHTML = `<div class="response-container">
                                                  <img src="${data.data.avatar_url}">
                                                  <p> Имя: <span>${data.data.name}</span><p>
                                                  <p> О себе: <span>${data.data.bio}</span><p>
                                                  <p> Кол-во репозиториев: <span>${data.data.public_repos}</span><p>
          </div>`) 
});

btnPlaceHolder.addEventListener('click', (e) => {
  e.preventDefault();
  axios.get(`https://jsonplaceholder.typicode.com/users`)
  .then(response => response.data)
  .then(data => {
    resultsContainer.innerHTML = data.map(item => ` <div class="response-container">    
                                                    <p> Имя: <span>${item.name}</span><p>
                                                    <p> Почта: <span>${item.email}</span><p>
                                                    <p> Номер телефона: <span>${item.phone}</span><p>
                                                    </div>`).join('')
  })
  })